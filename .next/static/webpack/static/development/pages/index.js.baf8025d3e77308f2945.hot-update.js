webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/tabla.js":
/*!*****************************!*\
  !*** ./components/tabla.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js";



var Tabla = function Tabla() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    },
    __self: this
  }, "Pokemon tipo fuego"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    class: "table",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, "Pokemon ID"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, "Nombre del Pokemon"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, "Imagen del pokemon"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, "Height"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "row",
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "1"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, "Mark"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "Otto"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-1134137357",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, "@mdo")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "1134137357",
    css: ".jsx-1134137357{background-color:#ffffff;margin-top:2%;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcQXBweUdhbWVyM1xcRG9jdW1lbnRzXFxIZWN0b3JcXFBva2VBcGlcXGNvbXBvbmVudHNcXHRhYmxhLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXFCb0IsQUFFcUMseUJBQ1osY0FDaEIiLCJmaWxlIjoiQzpcXFVzZXJzXFxBcHB5R2FtZXIzXFxEb2N1bWVudHNcXEhlY3RvclxcUG9rZUFwaVxcY29tcG9uZW50c1xcdGFibGEuanMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBUYWJsYSA9KCkgPT4oXHJcbiAgICA8ZGl2ID5cclxuICAgICAgICAgICAgPGgxPlBva2Vtb24gdGlwbyBmdWVnbzwvaDE+XHJcbiAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlXCI+XHJcbiAgICAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5Qb2tlbW9uIElEPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5Ob21icmUgZGVsIFBva2Vtb248L3RoPlxyXG4gICAgICAgICAgICAgICAgIDx0aCBzY29wZT1cImNvbFwiPkltYWdlbiBkZWwgcG9rZW1vbjwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+SGVpZ2h0PC90aD5cclxuICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJyb3dcIj4xPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGQ+TWFyazwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgPHRkPk90dG88L3RkPlxyXG4gICAgICAgICAgICAgICAgIDx0ZD5AbWRvPC90ZD5cclxuICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICAgIDwvdGFibGU+XHJcbiAgICAgICAgPHN0eWxlIGpzeD57YFxyXG4gICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgICAgbWFyZ2luLXRvcDoyJTtcclxuICAgICAgICBgfTwvc3R5bGU+XHJcbiAgICA8L2Rpdj5cclxuKTtcclxuZXhwb3J0IGRlZmF1bHQgVGFibGE7Il19 */\n/*@ sourceURL=C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js */",
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Tabla);

/***/ })

})
//# sourceMappingURL=index.js.baf8025d3e77308f2945.hot-update.js.map