webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/tabla.js":
/*!*****************************!*\
  !*** ./components/tabla.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/isomorphic-unfetch/browser.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js";



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var Tabla = function Tabla(props) {
  return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("p4", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, "Pokemon tipo fuego"), props.pokemons.id, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("table", {
    class: "table",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("thead", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, "Pokemon ID"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, "Nombre del Pokemon"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Imagen del pokemon"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, "Height"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, "weight"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "Primer Movimiento"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, "Base Experiencia"))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tbody", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("tr", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("th", {
    scope: "row",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, "1"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, "Mark"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "Otto"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "@mdo")))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
    styleId: "3022577989",
    css: ".jsx-3022577989{background-color:#ffffff;margin-top:2%;}p4.jsx-3022577989{font-size:14px;color:#00000;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcQXBweUdhbWVyM1xcRG9jdW1lbnRzXFxIZWN0b3JcXFBva2VBcGlcXGNvbXBvbmVudHNcXHRhYmxhLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQThCb0IsQUFFcUMsQUFJTixlQUNILFVBSkgsR0FLYixXQUNIIiwiZmlsZSI6IkM6XFxVc2Vyc1xcQXBweUdhbWVyM1xcRG9jdW1lbnRzXFxIZWN0b3JcXFBva2VBcGlcXGNvbXBvbmVudHNcXHRhYmxhLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEZldGNoIGZyb20gJ2lzb21vcnBoaWMtdW5mZXRjaCc7XHJcblxyXG5jb25zdCBUYWJsYSA9KHByb3BzKSA9PihcclxuICAgIDxkaXYgPlxyXG4gICAgICAgICAgICA8cDQ+UG9rZW1vbiB0aXBvIGZ1ZWdvPC9wND5cclxuICAgICAgICAgICAge3Byb3BzLnBva2Vtb25zLmlkfVxyXG4gICAgICAgICAgICA8dGFibGUgY2xhc3M9XCJ0YWJsZVwiPlxyXG4gICAgICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+UG9rZW1vbiBJRDwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+Tm9tYnJlIGRlbCBQb2tlbW9uPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5JbWFnZW4gZGVsIHBva2Vtb248L3RoPlxyXG4gICAgICAgICAgICAgICAgIDx0aCBzY29wZT1cImNvbFwiPkhlaWdodDwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+d2VpZ2h0PC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5QcmltZXIgTW92aW1pZW50bzwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+QmFzZSBFeHBlcmllbmNpYTwvdGg+XHJcbiAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICA8L3RoZWFkPlxyXG4gICAgICAgICAgICAgPHRib2R5PlxyXG4gICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwicm93XCI+MTwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRkPk1hcms8L3RkPlxyXG4gICAgICAgICAgICAgICAgIDx0ZD5PdHRvPC90ZD5cclxuICAgICAgICAgICAgICAgICA8dGQ+QG1kbzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgPHRkPkBtZG88L3RkPlxyXG4gICAgICAgICAgICAgICAgIDx0ZD5AbWRvPC90ZD5cclxuICAgICAgICAgICAgICAgICA8dGQ+QG1kbzwvdGQ+XHJcbiAgICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgICA8L3Rib2R5PlxyXG4gICAgICAgICAgICA8L3RhYmxlPlxyXG4gICAgICAgIDxzdHlsZSBqc3g+e2BcclxuICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgICAgIG1hcmdpbi10b3A6MiU7XHJcbiAgICAgICAgICAgcDRcclxuICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgY29sb3I6IzAwMDAwO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgICBgfTwvc3R5bGU+XHJcbiAgICA8L2Rpdj5cclxuKTtcclxuXHJcblxyXG5UYWJsYS5nZXRJbml0aWFsUHJvcHMgPSBhc3luYyBmdW5jdGlvbigpXHJcbntcclxuICAgIGNvbnN0IHJlc3B1ZXN0YSA9IGF3YWl0IEZldGNoKCdodHRwczovL3Bva2VhcGkuY28vYXBpL3YyL3R5cGUvMTAnKTtcclxuICAgIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwdWVzdGEuanNvbigpO1xyXG4gICAgcmV0dXJue1xyXG4gICAgICAgIHBva2Vtb25zOmRhdGFcclxuICAgIH07XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgVGFibGE7Il19 */\n/*@ sourceURL=C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js */",
    __self: this
  }));
};

Tabla.getInitialProps =
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
  var respuesta, data;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default()('https://pokeapi.co/api/v2/type/10');

        case 2:
          respuesta = _context.sent;
          _context.next = 5;
          return respuesta.json();

        case 5:
          data = _context.sent;
          return _context.abrupt("return", {
            pokemons: data
          });

        case 7:
        case "end":
          return _context.stop();
      }
    }
  }, _callee, this);
}));
/* harmony default export */ __webpack_exports__["default"] = (Tabla);

/***/ })

})
//# sourceMappingURL=index.js.588b630e2d3210d2e623.hot-update.js.map