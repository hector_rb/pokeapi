webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/tabla.js":
/*!*****************************!*\
  !*** ./components/tabla.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/isomorphic-unfetch/browser.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js";




var Tabla = function Tabla(props) {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p4", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, "Pokemon tipo fuego"), props.currency.disclaimer, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("table", {
    class: "table",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("thead", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, "Pokemon ID"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, "Nombre del Pokemon"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Imagen del pokemon"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, "Height"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, "weight"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "Primer Movimiento"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "col",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, "Base Experiencia"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tbody", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("tr", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("th", {
    scope: "row",
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, "1"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, "Mark"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "Otto"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, "@mdo"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("td", {
    className: "jsx-3022577989",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "@mdo")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "3022577989",
    css: ".jsx-3022577989{background-color:#ffffff;margin-top:2%;}p4.jsx-3022577989{font-size:14px;color:#00000;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcQXBweUdhbWVyM1xcRG9jdW1lbnRzXFxIZWN0b3JcXFBva2VBcGlcXGNvbXBvbmVudHNcXHRhYmxhLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQThCb0IsQUFFcUMsQUFJTixlQUNILFVBSkgsR0FLYixXQUNIIiwiZmlsZSI6IkM6XFxVc2Vyc1xcQXBweUdhbWVyM1xcRG9jdW1lbnRzXFxIZWN0b3JcXFBva2VBcGlcXGNvbXBvbmVudHNcXHRhYmxhLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtdW5mZXRjaCc7XHJcblxyXG5jb25zdCBUYWJsYSA9KHByb3BzKSA9PihcclxuICAgIDxkaXYgPlxyXG4gICAgICAgICAgICA8cDQ+UG9rZW1vbiB0aXBvIGZ1ZWdvPC9wND5cclxuICAgICAgICAgICAge3Byb3BzLmN1cnJlbmN5LmRpc2NsYWltZXJ9XHJcbiAgICAgICAgICAgIDx0YWJsZSBjbGFzcz1cInRhYmxlXCI+XHJcbiAgICAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5Qb2tlbW9uIElEPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5Ob21icmUgZGVsIFBva2Vtb248L3RoPlxyXG4gICAgICAgICAgICAgICAgIDx0aCBzY29wZT1cImNvbFwiPkltYWdlbiBkZWwgcG9rZW1vbjwvdGg+XHJcbiAgICAgICAgICAgICAgICAgPHRoIHNjb3BlPVwiY29sXCI+SGVpZ2h0PC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj53ZWlnaHQ8L3RoPlxyXG4gICAgICAgICAgICAgICAgIDx0aCBzY29wZT1cImNvbFwiPlByaW1lciBNb3ZpbWllbnRvPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJjb2xcIj5CYXNlIEV4cGVyaWVuY2lhPC90aD5cclxuICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgIDwvdGhlYWQ+XHJcbiAgICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICAgICA8dGggc2NvcGU9XCJyb3dcIj4xPC90aD5cclxuICAgICAgICAgICAgICAgICA8dGQ+TWFyazwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgPHRkPk90dG88L3RkPlxyXG4gICAgICAgICAgICAgICAgIDx0ZD5AbWRvPC90ZD5cclxuICAgICAgICAgICAgICAgICA8dGQ+QG1kbzwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgPHRkPkBtZG88L3RkPlxyXG4gICAgICAgICAgICAgICAgIDx0ZD5AbWRvPC90ZD5cclxuICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICAgIDwvdGFibGU+XHJcbiAgICAgICAgPHN0eWxlIGpzeD57YFxyXG4gICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgICAgbWFyZ2luLXRvcDoyJTtcclxuICAgICAgICAgICBwNFxyXG4gICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICBjb2xvcjojMDAwMDA7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICAgIDwvZGl2PlxyXG4pO1xyXG5cclxuXHJcblxyXG5leHBvcnQgZGVmYXVsdCBUYWJsYTsiXX0= */\n/*@ sourceURL=C:\\Users\\AppyGamer3\\Documents\\Hector\\PokeApi\\components\\tabla.js */",
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Tabla);

/***/ })

})
//# sourceMappingURL=index.js.c13d4aae754a4a344db2.hot-update.js.map