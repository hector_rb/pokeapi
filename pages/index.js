import Layout from '../components/layout';
import fetch from 'isomorphic-unfetch';

const Index = (props) => (
    <Layout>
    <div className="col-sm-11">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                         <div class="col-md-6">
                            <p1>Torres Cataluña Paquete: Lead Scoring</p1>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-md-auto" id="iconos">
                            <i class="fas fa-envelope"></i>
                            <i class="fas fa-bell"></i>
                            <i class="fas fa-cogs"></i>
                        </div>
                        <div class="col-md-auto">
                            Hola carlos<i class="fas fa-user-circle"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                    <table class="table">
                     <thead>
                       <tr>
                         <th scope="col">Pokemon ID</th>
                         <th scope="col">Nombre del Pokemon</th>
                         <th scope="col">Imagen del pokemon</th>
                         <th scope="col">Height</th>
                         <th scope="col">weight</th>
                         <th scope="col">Primer Movimiento</th>
                         <th scope="col">Base Experiencia</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <th scope="row">1</th>
                         <td>Mark</td>
                         <td>Otto</td>
                         <td>@mdo</td>
                         <td>@mdo</td>
                         <td>@mdo</td>
                         <td>@mdo</td>
                       </tr>
                     </tbody>
                    </table>
            </div>
        <style jsx>{`
            background-color: #F2F2F2;
            min-height: 100vh;
            padding-top:5%;
            padding-left:4%;
           p1{
               font-size:18px;
               color:#000000;
               font-weight:700;
           }
           #tabla
           {
            background-color: #5F4B8B;
           }
        `}</style>
    </div>
    </Layout>
);


Index.getInitialProps = async function()
{
    const respuesta = await fetch('https://pokeapi.co/api/v2/type/10');
    const data = await respuesta.json();
    return{
        pokemons: data
    };
}
export default Index;