import Navbar from '../components/navbar';
import Head from 'next/head';

const Layout = (props) => (
    <div> 
    <Head>
        <title>
            Prueba
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"/>

    </Head>
   
        <div class="row">
        <Navbar/>
             {props.children}
        </div>
        <style jsx>{`
            min-height: 100vh;  
            overflow-x: hidden;
            .row{
                width:100%;
            }      
        `}</style>
    </div>
);

export default Layout;